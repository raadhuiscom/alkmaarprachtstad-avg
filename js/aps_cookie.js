$( document ).ready(function() {

//shows cookie consent box if no cookie is set
//


if (!Cookies.get('aps-cookie') || Cookies.get('aps-cookie') == 'nieuw') {
	$('body').addClass('aps_offset');
	Cookies.set('aps-cookie','nieuw');
}


function checkRadioValues(){
	$('#aps_col_content_cookieitems').click(function(){
        var check = true;
        $('.ccm_btn_disabledmessage').hide();
        $("input:radio").each(function(){
            var name = $(this).attr("name");
            if($("input:radio[name="+name+"]:checked").length == 0){
               check = false;
            }
        });

        if(check){
            $("button").removeClass('ccm_btn_disabled')
        }else{
            $("button").addClass('ccm_btn_disabled')
        }
    });
}
checkRadioValues();

$("button[type='button']").click(function(){
			checkRadioValues();
			if($(this).hasClass('ccm_btn_disabled')) {
				$('.ccm_btn_disabledmessage').show();
			} else {
	            var socialValue = $("input[name='social']:checked").val();
	            var advertValue = $("input[name='advertising']:checked").val();
	            Cookies.set('aps-cookie-social',socialValue);
	            Cookies.set('aps-cookie-adv',advertValue);
	            Cookies.set('aps-cookie','voltooid');
	            $('body').removeClass('aps_offset');
        	}
        });


//Cookie uitleg toggle
$('#aps_toggle_extended--open').click(function(e){
	e.preventDefault();
	$(this).toggle();
	$('#aps_toggle_extended--close').toggle();
	$('#aps_col_content_extended').addClass('ccm_col_content_extended--open');
})

$('#aps_toggle_extended--close').click(function(e){
	e.preventDefault();
	$(this).toggle();
	$('#aps_toggle_extended--open').toggle();
	$('#aps_col_content_extended').removeClass('ccm_col_content_extended--open');
})



});